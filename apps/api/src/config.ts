enum DatabaseEngines {
  MySql = 'mysql',
  MongoDB = 'mongodb',
}

enum Databases {
  Default = 'default',
}

type DatabaseConfig = {
  type: DatabaseEngines;
  host: string;
  port: number;
  database: string;
  username: string;
  password: string;
  root_username: string;
  root_password: string;
};

type NestAppConfig = {
  env: string;
  port: number;
  debugger: number;
  databases: {
    [key in Databases]: DatabaseConfig;
  };
};

export const nestAppConfig: NestAppConfig = {
  env: process.env.NODE_ENV || 'development',
  port: Number(process.env.NEST_APP_PORT) || 5000,
  debugger: Number(process.env.NEST_APP_PORT_DEBUGGER) || 9229,
  databases: {
    default: {
      type: DatabaseEngines.MySql,
      host: process.env.DB_DEFAULT_HOST || '127.0.0.1',
      port: Number(process.env.DB_DEFAULT_PORT) || 3306,
      database: process.env.DB_DEFAULT_DATABASE || 'dev_db',
      username: process.env.DB_DEFAULT_USERNAME || 'dev_user',
      password: process.env.DB_DEFAULT_PASSWORD || 'qwertyuiop',
      root_username: process.env.DB_DEFAULT_ROOT_USERNAME || 'root',
      root_password: process.env.DB_DEFAULT_ROOT_PASSWORD || 'poiuytrewq',
    },
  },
};
