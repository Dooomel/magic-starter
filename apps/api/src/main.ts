import { NestFactory } from '@nestjs/core';
import * as helmet from 'helmet';
import { SwaggerModule, DocumentBuilder } from '@nestjs/swagger';
import { AppModule } from './app.module';
import { nestAppConfig } from './config';

async function bootstrap() {
  const app = await NestFactory.create(AppModule);

  const { port } = nestAppConfig;

  app.use(helmet());
  app.enableCors({
    origin: true,
    methods: 'GET,HEAD,PUT,PATCH,POST,DELETE,OPTIONS',
    credentials: true,
  });

  const options = new DocumentBuilder()
    .setTitle('ArgoWarehouse')
    .setDescription('ArgoWarehouse API description')
    .setVersion('1.0')
    .addBasicAuth()
    .addOAuth2()
    .addBearerAuth()
    .build();
  const document = SwaggerModule.createDocument(app, options);
  SwaggerModule.setup('swagger', app, document, {
    swaggerOptions: { persistAuthorization: true },
  });

  await app.listen(port);
  // eslint-disable-next-line no-console
  console.log(`Listening on ${await app.getUrl()}`);
}
bootstrap();
