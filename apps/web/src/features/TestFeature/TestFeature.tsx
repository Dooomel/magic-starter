import React from 'react';
import TestComponent from 'components/TestComponent/TestComponent';

interface Props {}

const TestFeature = (_props: Props) => {
  return (
    <div>
      <p>Test feature</p>
      <TestComponent />
    </div>
  );
};

export default TestFeature;
