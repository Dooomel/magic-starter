import React from 'react';
import { Button } from '@web/ui';
import { testFn } from '@magic/typings';

function App() {
  return (
    <div>
      <Button label="Test" />
      <p onClick={() => testFn()}>SPAN</p>
    </div>
  );
}

export default App;
