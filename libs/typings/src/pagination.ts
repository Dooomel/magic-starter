export type Column<T> = Extract<keyof T, string>;
export type Order<T> = [Column<T>, 'ASC' | 'DESC'];
export type SortBy<T> = Order<T>[];

export interface PaginationParams<T> {
  rowsPerPage: number;
  page: number;
  sortBy?: SortBy<T>;
  search?: string;
}
export interface PaginationMeta<T> extends PaginationParams<T> {
  count: number;
  totalPages: number;
}

export interface PaginationLinks {
  first?: string;
  previous?: string;
  current: string;
  next?: string;
  last?: string;
}
export interface PaginatedResponse<T> {
  data: T[];
  meta: PaginationMeta<T>;
  links: PaginationLinks;
}

export interface PaginateQuery {
  page?: number;
  rowsPerPage?: number;
  sortBy?: [string, string][];
  search?: string;
  path: string;
}

export const ROUTE_PAGINATION_PARAMS = ':rowsPerPage/:page/:sortBy/:search';
