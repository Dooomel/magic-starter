export interface JwtPayload {
  id: string;
  email: string;
  exp: number;
  iat: number;
}

export interface JwtResponse<U> {
  user: U;
  accessToken: string;
}
